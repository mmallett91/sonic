package util;

import org.lwjgl.opengl.GL11;

/**
 * simple 3d vector class
 * add sub cross product and magnitude are available
 * @author matt
 *
 */
public class Vector3d {
	
	public double x;
	public double y;
	public double z;
	
	public Vector3d(double x, double y, double z){
		
		this.x = x;
		this.y = y;
		this.z = z;
		
	}
	
	public Vector3d cross(Vector3d v){
		
		Vector3d u = this;
		
		Vector3d s = new Vector3d(0,0,0);
		
		s.x = u.y * v.z - u.z * v.y;
		s.y = u.z * v.x - u.x * v.z;
		s.z = u.x * v.y - u.y * v.x;
		
		return s;
	}
	
	public Vector3d sub(Vector3d v){
		
		return new Vector3d(
			this.x - v.x,
			this.y - v.y,
			this.z - v.z
		);
		
	}
	
	public Vector3d add(Vector3d v){
		
		return new Vector3d(
			this.x + v.x,
			this.y + v.y,
			this.z + v.z
		);
		
	}
	
	public double mag(){
		return Math.sqrt(x*x + y*y + z*z);
	}
	
	public void draw(){
		GL11.glBegin(GL11.GL_POINTS);
			GL11.glColor3f(1, 0, 0);
			GL11.glVertex3d(x, y, z);
		GL11.glEnd();
	}
	
	public String toString(){
		
		return "<" + x + "," + y + "," + z + ">";
	}

}
