package util;

import edu.columbia.cs4160.sonic.view.RingDrawable;
import edu.columbia.cs4160.sonic.view.SkyDrawable;
import edu.columbia.cs4160.sonic.view.SonicDrawable;
import edu.columbia.cs4160.sonic.view.Sound;
import edu.columbia.cs4160.sonic.view.TileDrawable;
import edu.columbia.cs4160.sonic.view.TitleDrawable;

/**
 * kick off init events for texture and sound loading
 * @author matt
 *
 */
public class Init {
	
	public static void init(){
		SonicDrawable.init();
		TileDrawable.init();
		RingDrawable.init();
		SkyDrawable.init();
		TitleDrawable.init();
		Sound.init();
	}

}
