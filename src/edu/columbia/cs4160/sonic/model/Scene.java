package edu.columbia.cs4160.sonic.model;

import java.util.ArrayList;

import util.Vector3d;
import edu.columbia.cs4160.sonic.view.RingMeter;
import edu.columbia.cs4160.sonic.view.SkyDrawable;
import edu.columbia.cs4160.sonic.view.Sound;

/**
 * ties together all elements of the game scene
 * receives keyboard and time events and updates state on all
 * contained entities
 * @author matt
 *
 */
public class Scene {
	
	private ArrayList<Entity> entities;
	
	private ArrayList<Entity> tiles;
	
	private ArrayList<Entity> rings;
	
	private Sonic sonic;
	
	private double velocity;
	
	private static final double STANDARD_VELOCITY = 2.0;
	
	private SkyDrawable sky;
	
	private int ringCount;
	
	private boolean done;
	
	private RingMeter ringMeter;
	
	public static final int RINGS_TO_WIN = 120;
	
	
	public Scene(){
		entities = new ArrayList<Entity>();
//		tiles = new ArrayList<Entity>();
//		rings = new ArrayList<Entity>();
		velocity = 0; //STANDARD_VELOCITY;
		
		sky = new SkyDrawable();
		
		ringCount = 0;
		
		done = false;
		
		ringMeter = new RingMeter(ringCount);
	}
	
	
	
	public void init(){
		
//		for(int i=0; i<100; i+=2){
//		
//			entities.add(new Tile(new Vector3d(0.0, 0.0, 0.0 -i), this));
//	        entities.add(new Tile(new Vector3d(0.0, 0.0, -1.0 -i), this));
//	        entities.add(new Tile(new Vector3d(0.0, 0.0, -0.5 -i), this));
//	        entities.add(new Tile(new Vector3d(-0.5, 0.0, -1.5 -i), this));
//	        entities.add(new Tile(new Vector3d(0.5, 0.0, -2.0 -i), this));
//	        entities.add(new Tile(new Vector3d(0.5, 0.0, -1.0 -i), this));
//	        
//	        entities.add(new Tile(new Vector3d(0.0, 0.0, -1.5 - i), this));
//	        
//		}
//		
//		tiles.addAll(entities);
//		
//		for(int i=0; i<100; i+=1){
//			
//			Ring r1 = new Ring(new Vector3d(0.0, 0.0, 0.0 -i), this);
//			entities.add(r1);
//			rings.add(r1);
//			
//			Ring r2 = new Ring(new Vector3d(-0.5, 0.0, -2.0 -i), this);
//			entities.add(r2);
//			rings.add(r2);
//		}
		
		LevelBuilder builder = new LevelBuilder("res/level.txt", this);
		Level level = builder.build();
		
		tiles = level.getTiles();
		rings = level.getRings();
		
		entities.addAll(tiles);
		entities.addAll(rings);
		
        sonic = new Sonic();
        
        entities.add(sonic);
        
        Sound.music();
	}
	
	public void startVelocity(){
		velocity = STANDARD_VELOCITY;
	}
	
	public void draw(){
		
		sky.draw();
		
		ringMeter.draw();
		
		for(Entity e : entities){
			e.getDrawable().draw();
		}
		
	}
	
	public void tick(long delta){
		for(Entity e : entities){
			e.tick(delta);
		}
		
		checkSonicOnTile();
		checkRingCollect();
		
		if(sonic.getState() == Sonic.State.DEAD){
			velocity = 0;
		}
		
	}
	
	public void moveLeft(){
		sonic.moveLeft();
	}
	
	public void moveRight(){
		sonic.moveRight();
	}
	
	public void jump(){
		sonic.jump();
	}
	
	public void anyKey(){
		
		if(sonic.isDone()){
			done = true;
		}
		
	}
	
	public boolean isDone(){
		return done;
	}
	
	private void checkSonicOnTile(){
		
		boolean onTile = false;
		Vector3d pos = sonic.getPosition();
		
		for(Entity e : tiles){
			Tile tile = (Tile) e;
			
			onTile = tile.contains(pos);
			if(onTile) break;
		}
		
		sonic.setOnTile(onTile);
		
	}
	
	private void checkRingCollect(){
		
		for(Entity e : rings){
			Ring ring = (Ring) e;
			
			if(ring.tryCollect(sonic)){
				ringCount++;
				ringMeter.addRing();
			}
			
			if(ringCount >= RINGS_TO_WIN){
				Sound.win();
				velocity = 0;
				done = true;
			}
			
		}
		
	}
	
	public double getVelocity(){
		return velocity;
	}
	

}
