package edu.columbia.cs4160.sonic.model;

import util.Vector3d;
import edu.columbia.cs4160.sonic.model.Sonic.State;
import edu.columbia.cs4160.sonic.view.Drawable;
import edu.columbia.cs4160.sonic.view.RingDrawable;
import edu.columbia.cs4160.sonic.view.Sound;

/**
 * state of a ring in the game field
 * updates the state through ring collection and time step events
 * @author matt
 *
 */
public class Ring extends Entity {
	
	public static final double WIDTH = .05;
	public static final double HEIGHT = .05;
	
	private static final double Y = 0; 
	
	private static final long FRAME_LENGTH = 100;
	
	private Scene scene;
	
	private long frameDelta;
	private int frame;
	private RingDrawable drawable;
	
	private boolean collected;
	
	public Ring(Scene scene){
		this(new Vector3d(0.0, 0.0, 0.0), scene);
	}
	
	public Ring(Vector3d position, Scene scene){
		this.position = position;
		this.position.y = Y;
		
		frameDelta = 0;
		frame = 0;
		
		this.scene = scene;
		
		collected = false;
		
		drawable = new RingDrawable(this);
	}

	@Override
	public Drawable getDrawable() {
		return drawable;
	}

	@Override
	public void tick(long delta) {
		
		double velocity = scene.getVelocity();
		
		double seconds = delta / 1000.0;
		double distance = velocity * seconds;
		
		translate(new Vector3d(0.0, 0.0, distance));
		
		frameTick(delta);

	}
	
	public int getFrame() {
		return frame;
	}
	
	private void frameTick(long delta){
		
		frameDelta += delta;
		
		if(frameDelta >= FRAME_LENGTH){
			
			frame++;
			frame %= RingDrawable.NUM_TEXTURES;
			
			frameDelta = 0;
			
		}
	}
	
	public boolean tryCollect(Sonic sonic){
		
		boolean ret = false;
		
		if(!collected){
		
			Vector3d v = sonic.getPosition();
			
			ret = 
				(v.x < position.x + WIDTH / 2.0 + .05) &&
				(v.x > position.x - WIDTH / 2.0 - .05) &&
//				(v.y + Y > position.z - LENGTH / 2.0 - .02) &&
//				(v.y + Y < position.z + LENGTH / 2.0 + .02);
				(v.x < position.z + 0.2 && v.x < position.z - 0.2) &&
				(v.y < position.y + HEIGHT);
//				(v.y < position.y + HEIGHT);
			
//			State state = sonic.getState();
			
//			ret &= state != Sonic.State.FALLING && state != Sonic.State.DEAD;  
			
			collected = ret;
			
			if(ret){
				Sound.ring();
			}
			
		}
		
		return ret;
		
		
	}

	public boolean isCollected() {
		return collected;
	}
	
	
	
	
	
	
	
	
	
	
	

}
