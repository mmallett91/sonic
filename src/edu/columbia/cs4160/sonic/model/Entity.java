package edu.columbia.cs4160.sonic.model;

import util.Vector3d;
import edu.columbia.cs4160.sonic.view.Drawable;

/**
 * abstract representation of an entity in the game field
 * @author matt
 *
 */
public abstract class Entity {
	
	protected Vector3d position;
	
	public abstract Drawable getDrawable();
	
	public abstract void tick(long delta);
	
	public Vector3d getPosition(){
		return position;
	}
	
	public void translate(Vector3d translate){
		
		position = position.add(translate);
		
	}

}
