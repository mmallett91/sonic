package edu.columbia.cs4160.sonic.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import util.Vector3d;

/**
 * parses a level file and outputs a level to be played through
 * with scene class
 * @author matt
 *
 */
public class LevelBuilder {
	
	private Scanner file;
	
	private ArrayList<Entity> tiles;
	private ArrayList<Entity> rings;
	
	private Scene scene;
	
	private static final double X_MULT = 0.5;
	private static final double Z_MULT = -0.5;
	
	
	private static final int TYPE_PLAIN = 0;
	private static final int TYPE_ONE_RING = 1;
	private static final int TYPE_TWO_RING_VERT = 2;
	private static final int TYPE_TWO_RING_HORZ = 3;
	private static final int TYPE_FOUR_RING = 4;
	
	public LevelBuilder(String filePath, Scene scene){
		
		try {
			file = new Scanner(new File(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tiles = new ArrayList<Entity>();
		rings = new ArrayList<Entity>();
		
		this.scene = scene;
		
	}
	
	public Level build(){
		
		while(file.hasNextLine()){
			
			
			String line = file.nextLine();
			
			processLine(line);
			
		}
		
		return new Level(tiles, rings);
		
		
	}
	
	private void processLine(String line){
		
		
		Scanner parser = new Scanner(line);
		
		double x = parser.nextDouble() * X_MULT;
		double z = parser.nextDouble() * Z_MULT;
		
		int type = parser.nextInt();
		
		Tile tile = new Tile(new Vector3d(x, 0.0, z), scene);
		tiles.add(tile);
		
		switch(type){
		case TYPE_PLAIN:
			break;
		case TYPE_ONE_RING:
			doOneRing(x, z);
			break;
		case TYPE_TWO_RING_VERT:
			doTwoRingVert(x, z);
			break;
		case TYPE_TWO_RING_HORZ:
			doTwoRingHorz(x, z);
			break;
		case TYPE_FOUR_RING:
			doFourRing(x, z);
			break;
		
		}
		
	}
	
	
	private void doOneRing(double x, double z){
		
		Ring ring = new Ring(new Vector3d(x, 0.0, z), scene);
		rings.add(ring);
	}
	
	private void doTwoRingVert(double x, double z){
		double zOffset = Tile.LENGTH / 4.0;
		
		Ring ring1 = new Ring(new Vector3d(x, 0.0, z + zOffset), scene);
		Ring ring2 = new Ring(new Vector3d(x, 0.0, z - zOffset), scene);
		
		rings.add(ring1);
		rings.add(ring2);
	}
	
	private void doTwoRingHorz(double x, double z){
		
		double xOffset = Tile.WIDTH / 8.0;
		
		Ring ring1 = new Ring(new Vector3d(x + xOffset, 0.0, z), scene);
		Ring ring2 = new Ring(new Vector3d(x - xOffset, 0.0, z), scene);
		
		rings.add(ring1);
		rings.add(ring2);
	}
	
	private void doFourRing(double x, double z){
		
	}

}





























