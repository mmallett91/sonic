package edu.columbia.cs4160.sonic.model;

import util.Vector3d;
import edu.columbia.cs4160.sonic.view.Drawable;
import edu.columbia.cs4160.sonic.view.TileDrawable;

/**
 * state of a tile of dirt in the game field
 * updates state through time step events
 * @author matt
 *
 */
public class Tile extends Entity {
	
	public static final double LENGTH = 0.5;
	public static final double WIDTH = 0.5;
	public static final double HEIGHT = 0.2;
	
	private Scene scene;
	
	private Drawable drawable;
	
	public Tile(Scene scene){
		this(new Vector3d(0.0, 0.0, 0.0), scene);
	}
	
	public Tile(Vector3d position, Scene scene){
		this.position = position;
		this.scene = scene;
		drawable = new TileDrawable(this);
	}
	
	@Override
	public Drawable getDrawable() {
		
		return drawable;
	}

	@Override
	public void tick(long delta) {
		
		double velocity = scene.getVelocity();
		
		double seconds = delta / 1000.0;
		double distance = velocity * seconds;
		
		translate(new Vector3d(0.0, 0.0, distance));
		
	}
	
	public boolean contains(Vector3d v){
		
		return 
			(v.x < position.x + WIDTH / 2.0 + .02) &&
			(v.x > position.x - WIDTH / 2.0 - .02) &&
			(v.z > position.z - LENGTH / 2.0 - .02) &&
			(v.z < position.z + LENGTH / 2.0 + .02);
		
		
	}
	

}
