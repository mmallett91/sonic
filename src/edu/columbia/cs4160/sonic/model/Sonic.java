package edu.columbia.cs4160.sonic.model;

import util.Vector3d;
import edu.columbia.cs4160.sonic.view.Drawable;
import edu.columbia.cs4160.sonic.view.SonicDrawable;
import edu.columbia.cs4160.sonic.view.Sound;

/**
 * state of sonic within the game field
 * handles frame animation and movement events
 * as well as jump and falling
 * 
 * @author matt
 *
 */
public class Sonic extends Entity {
	
	private static final double Z = 0;
	
	private static final double GRAVITY = 9.8;
	
	private static final double JUMP_VELOCITY = 3.0;
	private static final double SIDE_VELOCITY = 0.01;
	
//	public static final double LENGTH = .2;
	public static final double WIDTH = .13;
	public static final double HEIGHT = .20;
	
	private SonicDrawable drawable;
	
	private boolean done;
	
	public enum State {
		RUNNING,
		JUMPING,
		FALLING,
		DEAD
	}
	
	private State state;
	
	private long jumpDelta;
	
	private static final long FRAME_LENGTH = 40;
	
	private long frameDelta;
	
	private int frame;

	private boolean onTile;
	
	
	public Sonic(){
		this(new Vector3d(0.0, 0.0, 0.0));
	}
	
	public Sonic(Vector3d position){
		this.position = position;
		this.position.z = Z;
		state = State.RUNNING;
		jumpDelta = 0;
		
		frameDelta = 0;
		frame = 0;
		
		onTile = true;
		
		done = false;
		
		drawable = new SonicDrawable(this);
	}

	@Override
	public Drawable getDrawable() {
		return drawable;
		//want this one to be long lived, hold some state
	}
	
	@Override
	public void translate(Vector3d t){
		super.translate(t);
		position.z = Z;
	}

	@Override
	public void tick(long delta) {
		
		if(state == State.RUNNING){
			
			if(!onTile){
				fall();
			}
		}
		
		if(state == State.JUMPING){
			jumpDelta += delta;
			double displacement = getJumpDisplacement();
			if(displacement > 0.0){
				position.y = displacement;
			}
			else if(onTile){
				
				position.y = 0.0;
				state = State.RUNNING;
				
				frameReset();
			}
			else{
				fall();
			}
			
		}
		
		if(state == State.FALLING){
			
			if(position.y > -.3){
				position.y -= .02;
			}
			else{
				die();
			}
			
		}
		
		if(state == State.DEAD){
			
			if(position.y < .2){
				position.y += .05;
			}
			else{
				done = true;
			}
			
		}
		
		
		frameTick(delta);
		
		
	}
	
	public State getState(){
		return state;
	}
	
	public int getFrame(){
		return frame;
	}
	
	public void setOnTile(boolean onTile){
		this.onTile = onTile;
	}

	public void moveLeft() {
		if(state != State.DEAD){
			translate(new Vector3d(-SIDE_VELOCITY, 0.0, 0.0));
		}
		
	}

	public void moveRight() {
		if(state != State.DEAD){
			translate(new Vector3d(SIDE_VELOCITY, 0.0, 0.0));
		}
		
	}

	public void jump() {
		
		if(state == State.RUNNING){
			state = State.JUMPING;
			jumpDelta = 0;
			
			frameReset();
			Sound.jump();
		}
		
	}
	
	public boolean isDone(){
		return done;
	}
	
	private void fall(){
		
		state = State.FALLING;
		
	}
	
	private void die(){
		
		state = State.DEAD;
		
		Sound.die();
		
	}
	
	private double getJumpDisplacement(){
		
		double dJumpDelta = ((double) jumpDelta) / 1000.0;
		
		return JUMP_VELOCITY * dJumpDelta - .5 * GRAVITY * dJumpDelta * dJumpDelta;
	}
	
	private void frameReset(){
		frameDelta = 0;
		frame = 0;
	}
	
	private void frameTick(long delta){
		
		frameDelta += delta;
		
		if(frameDelta >= FRAME_LENGTH){
			
			int mod = (state == State.RUNNING) ? SonicDrawable.NUM_RUN_TEXTURES : SonicDrawable.NUM_JUMP_TEXTURES; 
			
			frame++;
			frame %= mod;
			
			frameDelta = 0;
			
		}
		
		
	}
	

}
