package edu.columbia.cs4160.sonic.model;

import java.util.ArrayList;

/**
 * the result of parsing and processing a level file
 * contains ring and tile locations
 * @author matt
 *
 */
public class Level {
	
	private ArrayList<Entity> rings;
	private ArrayList<Entity> tiles;
	
	public Level(ArrayList<Entity> tiles, ArrayList<Entity> rings){
		
		this.tiles = tiles;
		this.rings = rings;
		
	}
	
	public ArrayList<Entity> getRings(){
		return rings;
	}
	
	public ArrayList<Entity> getTiles(){
		return tiles;
	}

}
