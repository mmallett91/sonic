package edu.columbia.cs4160.sonic;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import util.Init;
import edu.columbia.cs4160.sonic.model.Scene;
import edu.columbia.cs4160.sonic.view.TitleDrawable;

/**
 * bring everything together
 * starts and inits opengl
 * runs render loop
 * gets keyboard and time input
 * @author matt
 *
 */
public class SonicApplication {

    String windowTitle = "Sonic the Hedgehog";
    public boolean closeRequested = false;

    EventQueue queue;
    
    private Scene scene;
    
    private long time;
    
    private boolean title;

    public void run() {

        createWindow();
        initGL();
        
        Init.init();
        
        scene = new Scene();
        scene.init();
        
        queue = new EventQueue();
        
        time = System.currentTimeMillis();
        
        title = true;
        
        while (!closeRequested) {
            pollInput();
            tick();
            
            queue.handle();
            
			if(scene.isDone()){
				title = true;
				scene = new Scene();
				scene.init();
            }
            
            renderGL();

            Display.update();
        }
        
        cleanup();
    }
    
    private void initGL() {

        /* OpenGL */
        int width = Display.getDisplayMode().getWidth();
        int height = Display.getDisplayMode().getHeight();

        GL11.glViewport(0, 0, width, height); // Reset The Current Viewport
        GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        GL11.glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 100.0f); // Calculate The Aspect Ratio Of The Window
        GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        GL11.glLoadIdentity(); // Reset The Modelview Matrix

        GL11.glShadeModel(GL11.GL_SMOOTH); // Enables Smooth Shading
        GL11.glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // white Background
        GL11.glClearDepth(1.0f); // Depth Buffer Setup
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
        GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Test To Do
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // Really Nice Perspective Calculations
        
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
    }
    
    


    private void renderGL() {

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL11.glLoadIdentity(); // Reset The View
        GL11.glTranslatef(0.0f, -0.2f, -1.0f); // Move Right And Into The Screen
        
        if(title){
        	TitleDrawable title = new TitleDrawable();
        	title.draw();
        }
        else{
        	scene.draw();
        }
        
    }

    /**
     * Poll Input
     */
    public void pollInput() {
        // scroll through key events
    	
    	if(title){
    		if(Keyboard.next()){
    			scene.startVelocity();
    			title = false;
    		}
    	}
    	
    	else{
	        while (Keyboard.next()) {
	            if (Keyboard.getEventKeyState()) {
	                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
	                    closeRequested = true;
	                else{
	                	queue.enqueue(new KeyEvent(Keyboard.getEventKey(), scene));
	                }
	            }
	        }
	        
	        if(Keyboard.isKeyDown(Keyboard.KEY_A)){
	        	queue.enqueue(new KeyEvent(Keyboard.KEY_A, scene));
	        }
	        if(Keyboard.isKeyDown(Keyboard.KEY_D)){
	        	queue.enqueue(new KeyEvent(Keyboard.KEY_D, scene));
	        }
    	}

        if (Display.isCloseRequested()) {
            closeRequested = true;
        }
    }
    
    
    private void tick(){
    	long tmp = System.currentTimeMillis();
    	long delta = tmp - time;
    	time = tmp;
    	queue.enqueue(new TickEvent(delta, scene));
    }
    
    
    private void createWindow() {
        try {
            Display.setDisplayMode(new DisplayMode(960, 540));
            Display.setVSyncEnabled(true);
            Display.setTitle(windowTitle);
            Display.create();
        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }
    
    /**
     * Destroy and clean up resources
     */
    private void cleanup() {
        Display.destroy();
    }
    
    public static void main(String[] args) {
        new SonicApplication().run();
    }
    
   
}
