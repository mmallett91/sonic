package edu.columbia.cs4160.sonic;

import edu.columbia.cs4160.sonic.model.Scene;

/**
 * event generated by measuring delta since last render loop
 * passed in to scene which updates its state with the time step
 * @author matt
 *
 */
public class TickEvent extends Event {
	
	private long delta;
	private Scene scene;
	
	public TickEvent(long delta, Scene scene){
		this.delta = delta;
		this.scene = scene;
	}

	@Override
	public void handle() {
		scene.tick(delta);

	}

}
