package edu.columbia.cs4160.sonic;

import java.util.ArrayDeque;

/**
 * implements a queue to allow events to be passed in as generated
 * and subsequently handled
 * @author matt
 *
 */
public class EventQueue {
	
	
	private ArrayDeque<Event> events;
	
	
	public EventQueue(){
		events = new ArrayDeque<Event>();
	}
	
	public void enqueue(Event e){
		events.add(e);
	}
	
	public void handle(){
		for(Event e : events){
			e.handle();
		}
		events.clear();
	}

}
