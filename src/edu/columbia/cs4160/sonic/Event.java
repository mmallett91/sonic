package edu.columbia.cs4160.sonic;

/**
 * abstract representation of an event generated by user
 * passed into the event queue for processing
 * @author matt
 *
 */
public abstract class Event{
	
	public abstract void handle();

}
