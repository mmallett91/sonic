package edu.columbia.cs4160.sonic.view;

/**
 * interface that enables this entity to be drawn
 * @author matt
 *
 */
public interface Drawable {
	
	public void draw();

}
