package edu.columbia.cs4160.sonic.view;

import org.lwjgl.opengl.GL11;

import edu.columbia.cs4160.sonic.model.Scene;

/**
 * draws the ring meter on the canvas
 * as more rings are collected the percentage completed
 * is updated on the ui
 * @author matt
 *
 */
public class RingMeter implements Drawable{

	private int rings;
	
	public RingMeter(int rings){
		this.rings = rings;
	}
	
	public void addRing(){
		rings++;
	}
	
	@Override
	public void draw() {
		// TODO Auto-generated method stub
		
		float x = (1f - (rings / (float) Scene.RINGS_TO_WIN)) * .7f;
		
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		
		GL11.glBegin(GL11.GL_QUADS);
		
			//draw meter background
			GL11.glColor4f(1f, 1f, 1f, .5f);
			
			GL11.glVertex3f(-.7f, .56f, 0);
			GL11.glVertex3f(.7f, .56f, 0);
			GL11.glVertex3f(.7f, .6f, 0);
			GL11.glVertex3f(-.7f, .6f, 0);
			
			GL11.glColor4f(1f,  1f, 1f, 1f);
			
			GL11.glVertex3f(-x, .56f, 0);
			GL11.glVertex3f(0, .56f, 0);
			GL11.glVertex3f(0, .6f, 0);
			GL11.glVertex3f(-x, .6f, 0);
			
			GL11.glVertex3f(0, .56f, 0);
			GL11.glVertex3f(x, .56f, 0);
			GL11.glVertex3f(x, .6f, 0);
			GL11.glVertex3f(0, .6f, 0);
		
		GL11.glEnd();
		
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		
	}

}
