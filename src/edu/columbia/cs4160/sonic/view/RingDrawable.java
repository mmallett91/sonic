package edu.columbia.cs4160.sonic.view;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import util.Vector3d;
import edu.columbia.cs4160.sonic.model.Ring;
import edu.columbia.cs4160.sonic.model.Sonic;
import edu.columbia.cs4160.sonic.model.Sonic.State;

/**
 * takes the state of its ring and draws it on the canvas
 * also handles loading of ring textures
 * @author matt
 *
 */
public class RingDrawable implements Drawable {
	
	public static final int NUM_TEXTURES = 5; 
	
	private static Texture[] textures;
	
	private Ring ring;
	
	private static int index;
	
	public RingDrawable(Ring ring){
		this.ring = ring;
	}
	
	private static Vector3d offset;
	static{
		offset = new Vector3d(
			-Ring.WIDTH/2.0,
			0.0,
			0.0
		);
	}

	@Override
	public void draw() {
		
		if(!ring.isCollected()){
			
			Vector3d v = ring.getPosition();
			
			GL11.glPushMatrix();
			GL11.glTranslated(v.x, v.y, v.z);
			GL11.glTranslated(offset.x, offset.y, offset.z);
			
			int frame = ring.getFrame();
			
			Texture t = textures[frame];
			
			
	//		Texture t = (sonic.getState() == State.RUNNING) ? runTextures[frame] : jumpTextures[frame];
			
	//		GL11.glEnable(GL11.GL_TEXTURE_2D);
			t.bind();
			
			GL11.glCallList(index);
			
	//		GL11.glDisable(GL11.GL_TEXTURE_2D);
			
			
			GL11.glPopMatrix();
		}

	}
	
	
	public static void init(){
		
		textures = new Texture[NUM_TEXTURES];
		
		for(int i=0; i<textures.length; i++){
			try {
				textures[i] = TextureLoader.getTexture(
						"PNG", ResourceLoader.getResourceAsStream("textures/ring" + (i+1) + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
		}
		
		index = GL11.glGenLists(1);
		GL11.glNewList(index, GL11.GL_COMPILE);
		
		GL11.glBegin(GL11.GL_QUADS);
		
		
			GL11.glColor4d(1, 1, 1, 1);
			
				//front
				GL11.glTexCoord2f(1, 1);
				GL11.glVertex3d(0.0, 0.0, 0.0);
				
				GL11.glTexCoord2f(0, 1);
				GL11.glVertex3d(Ring.WIDTH, 0.0, 0.0);
				
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3d(Ring.WIDTH, Ring.HEIGHT, 0.0);
				
				
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex3d(0.0, Ring.HEIGHT, 0.0);
			
		
			GL11.glEnd();
		
		GL11.glEndList();
		
		
	}

}
