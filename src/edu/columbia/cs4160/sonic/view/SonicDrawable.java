package edu.columbia.cs4160.sonic.view;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import util.Vector3d;
import edu.columbia.cs4160.sonic.model.Sonic;
import edu.columbia.cs4160.sonic.model.Sonic.State;

/**
 * draw sonic on the canvas
 * get the frame from state and select the correct texture
 * to produce frame animation
 * 
 * also loads all sonic sprite textures
 * @author matt
 *
 */
public class SonicDrawable implements Drawable {
	
	private Sonic sonic;
	
	public static final int NUM_RUN_TEXTURES = 6;
	public static final int NUM_JUMP_TEXTURES= 5;
	
	private static Texture[] runTextures;
	private static Texture[] jumpTextures;
	private static Texture deadTexture;
	
	private static int index;
	
	
	private static Vector3d offset;
	static{
		offset = new Vector3d(
			-Sonic.WIDTH/2.0,
			0.0,
			0.0
		);
		
	}
	
	public SonicDrawable(Sonic sonic){
		this.sonic = sonic;
	}

	@Override
	public void draw() {
		
		Vector3d v = sonic.getPosition();
		
		GL11.glPushMatrix();
		GL11.glTranslated(v.x, v.y, v.z);
		GL11.glTranslated(offset.x, offset.y, offset.z);
		
		int frame = sonic.getFrame();
		
		Texture t;
		
		if(sonic.getState() == State.RUNNING){
			t = runTextures[frame];
		}
		else if(sonic.getState() == State.JUMPING || sonic.getState() == State.FALLING){
			t = jumpTextures[frame % NUM_JUMP_TEXTURES];
		}
		else{
			t = deadTexture;
		}
		
//		Texture t = (sonic.getState() == State.RUNNING) ? runTextures[frame] : jumpTextures[frame];
		
//		GL11.glEnable(GL11.GL_TEXTURE_2D);
		t.bind();
		
		GL11.glCallList(index);
		
//		GL11.glDisable(GL11.GL_TEXTURE_2D);
		
		
		GL11.glPopMatrix();

	}
	
	public static void init(){
		runTextures = new Texture[NUM_RUN_TEXTURES];
		jumpTextures = new Texture[NUM_JUMP_TEXTURES];
		
		for(int i=0; i<runTextures.length; i++){
			try {
				runTextures[i] = TextureLoader.getTexture(
						"PNG", ResourceLoader.getResourceAsStream("textures/Run" + (i+1) + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(int i=0; i<jumpTextures.length; i++){
			try {
				jumpTextures[i] = TextureLoader.getTexture(
						"PNG", ResourceLoader.getResourceAsStream("textures/Ball" + (i+1) + ".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			deadTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("textures/dead.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		index = GL11.glGenLists(1);
		
		GL11.glNewList(index, GL11.GL_COMPILE);
		
			GL11.glBegin(GL11.GL_QUADS);
			
				
				GL11.glColor4d(1, 1, 1, 1);
				
				//front
				GL11.glTexCoord2f(1, 1);
				GL11.glVertex3d(0.0, 0.0, 0.0);
				
				GL11.glTexCoord2f(0, 1);
				GL11.glVertex3d(Sonic.WIDTH, 0.0, 0.0);
				
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3d(Sonic.WIDTH, Sonic.HEIGHT, 0.0);
				
				
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex3d(0.0, Sonic.HEIGHT, 0.0);
			
	
			GL11.glEnd();
			
		GL11.glEndList();
		
		
	}

}
