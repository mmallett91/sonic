package edu.columbia.cs4160.sonic.view;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import util.Vector3d;
import edu.columbia.cs4160.sonic.model.Tile;

/**
 * draws a tile on the canvas from its state in the Tile class
 * 
 * also loads tile textures
 * @author matt
 *
 */
public class TileDrawable implements Drawable{
	
	private Tile tile;
	
	private static Texture grass;
	private static Texture grassSide;
	
	private static int list1, list2;
	
	//makes sense for model to be based on center
	//and for renderer to be based on a corner
	private static Vector3d offset;
	static{
		offset = new Vector3d(
			-Tile.WIDTH/2.0,
			0.0,
			Tile.LENGTH/2.0
		);
		
	}
	
	public TileDrawable(Tile tile){
		this.tile = tile;
	}

	@Override
	public void draw() {
		
		Vector3d v = tile.getPosition();
		
		GL11.glPushMatrix();
		GL11.glTranslated(v.x, v.y, v.z);
		GL11.glTranslated(offset.x, offset.y, offset.z);
		
		grass.bind();
		
		GL11.glCallList(list1);
		
		grassSide.bind();
		
		GL11.glCallList(list2);
		
		GL11.glPopMatrix();
		
		
	}
	
	public static void init(){
		
		try {
			grass = TextureLoader.getTexture("JPG", ResourceLoader.getResourceAsStream("textures/grass.jpg"));
			grassSide = TextureLoader.getTexture("JPG", ResourceLoader.getResourceAsStream("textures/grass_dirt.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list1 = GL11.glGenLists(1);
		
		GL11.glNewList(list1, GL11.GL_COMPILE);
		
			GL11.glBegin(GL11.GL_QUADS);
				
		//		GL11.glColor3f(0.2f, 1.0f, 0.2f);
			
				//top
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3d(0.0, 0.0, 0.0);
				
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex3d(Tile.WIDTH, 0.0, 0.0);
				
				GL11.glTexCoord2f(1, 1);
				GL11.glVertex3d(Tile.WIDTH, 0.0, -Tile.LENGTH);
				
				GL11.glTexCoord2f(0, 1);
				GL11.glVertex3d(0.0, 0.0, -Tile.LENGTH);
			
			GL11.glEnd();
		
		GL11.glEndList();
		
		list2 = GL11.glGenLists(1);
		
		GL11.glNewList(list2,  GL11.GL_COMPILE);
		
			GL11.glBegin(GL11.GL_QUADS);
				
				//bottom
		//		GL11.glVertex3d(0.0, -Tile.HEIGHT, 0.0);
		//		GL11.glVertex3d(Tile.WIDTH, -Tile.HEIGHT, 0.0);
		//		GL11.glVertex3d(Tile.WIDTH, -Tile.HEIGHT, -Tile.LENGTH);
		//		GL11.glVertex3d(0.0, -Tile.HEIGHT, -Tile.LENGTH);
				
		//		GL11.glColor3f(.667f, .447f, .263f);
			
				//left
				GL11.glTexCoord2f(.5f, 0);
				GL11.glVertex3d(0.0, 0.0, 0.0);
				
				GL11.glTexCoord2f(.5f, .5f);
				GL11.glVertex3d(0.0, -Tile.HEIGHT, 0.0);
				
				GL11.glTexCoord2f(0, .5f);
				GL11.glVertex3d(0.0, -Tile.HEIGHT, -Tile.LENGTH);
				
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3d(0.0, 0.0, -Tile.LENGTH);
				
				//right
				GL11.glTexCoord2f(.5f, 0);
				GL11.glVertex3d(Tile.WIDTH, 0.0, 0.0);
				
				GL11.glTexCoord2f(.5f, .5f);
				GL11.glVertex3d(Tile.WIDTH, -Tile.HEIGHT, 0.0);
				
				GL11.glTexCoord2f(0, .5f);
				GL11.glVertex3d(Tile.WIDTH, -Tile.HEIGHT, -Tile.LENGTH);
				
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3d(Tile.WIDTH, 0.0, -Tile.LENGTH);
				
				//front
				GL11.glTexCoord2f(0, 0);
				GL11.glVertex3d(0.0, 0.0, 0.0);
				
				GL11.glTexCoord2f(1, 0);
				GL11.glVertex3d(Tile.WIDTH, 0.0, 0.0);
				
				GL11.glTexCoord2f(1, .5f);
				GL11.glVertex3d(Tile.WIDTH, -Tile.HEIGHT, 0.0);
				
				GL11.glTexCoord2f(0, .5f);
				GL11.glVertex3d(0.0, -Tile.HEIGHT, 0.0);
		
			GL11.glEnd();
		
		GL11.glEndList();
	}

}
