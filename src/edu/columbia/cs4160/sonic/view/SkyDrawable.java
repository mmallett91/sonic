package edu.columbia.cs4160.sonic.view;

import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import edu.columbia.cs4160.sonic.model.Sonic;

/**
 * draw the sky background
 * load sky texture
 * @author matt
 *
 */
public class SkyDrawable implements Drawable {
	
	private static Texture texture;

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		
		texture.bind();
		
		GL11.glBegin(GL11.GL_QUADS);
		
			GL11.glColor4d(1, 1, 1, 1);
			
			//front
			GL11.glTexCoord2f(1, 1);
			GL11.glVertex3d(-8.0, -4.0, -8.0);
			
			GL11.glTexCoord2f(0, 1);
			GL11.glVertex3d(8.0, -4.0, -8.0);
			
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex3d(8.0, 4.0, -8.0);
			
			
			GL11.glTexCoord2f(1, 0);
			GL11.glVertex3d(-8.0, 4.0, -8.0);
		
		GL11.glEnd();
		
	}
	
	public static void init(){
		
		try {
			texture = TextureLoader.getTexture(
					"PNG", ResourceLoader.getResourceAsStream("textures/sky_pixels_2.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
