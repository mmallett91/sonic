package edu.columbia.cs4160.sonic.view;

import java.io.IOException;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 * load and play sound files!
 * @author matt
 *
 */
public class Sound {
	
	private static Audio ring;
	private static Audio jump;
	private static Audio die;
	private static Audio win;
	private static Audio music;
	
	public static void init(){
		
		
		try {
			ring = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sfx/ring.wav"));
			jump = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sfx/jump.wav"));
			die = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sfx/die.wav"));
			win = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sfx/win.wav"));
			music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/sfx/green_hill_zone.wav"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void ring(){
		
		ring.playAsSoundEffect(1.0f, 1.0f, false);
		
	}
	
	public static void jump(){
		
		jump.playAsSoundEffect(1.0f, 1.0f, false);
	}
	
	public static void die(){
		
		die.playAsSoundEffect(1.0f, 1.0f, false);
		
	}
	
	public static void win(){
		
		win.playAsSoundEffect(1.0f, 0.2f, false);
		
	}
	
	public static void music(){
		
		music.playAsMusic(1.0f, 1.0f, true);
		
	}

}
